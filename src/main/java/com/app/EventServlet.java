package com.app;


import data.access.MyEntityManager;
import models.Event;
import models.Path;
import models.User;
import models.UserEvent;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import javax.persistence.EntityManager;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;

@WebServlet("/event")
public class EventServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        EntityManager entityManager = MyEntityManager.getEntityManager();

        entityManager.getTransaction().begin();

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        java.util.Date startDate = null;
        java.util.Date endDate = null;
        try {
            startDate = format.parse(request.getParameter("startTime"));
            endDate = format.parse(request.getParameter("endTime"));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Timestamp sqlStartDate = new Timestamp(startDate.getTime());
        Timestamp sqlEndDate = new Timestamp(endDate.getTime());
        User owner = (User)request.getSession(true).getAttribute("user");
        Event event = new Event();
        event.setOwnerId(owner.getId());
        event.setEventDescription(request.getParameter("description"));
        event.setScheduledTime(sqlStartDate);
        event.setExtepectedEndTime(sqlEndDate);
        event.setStatusId(1);
        event.setRouteId(Integer.valueOf(request.getParameter("route")));

        entityManager.persist(event);

        entityManager.getTransaction().commit();
        MyEntityManager.closeEntityManagerFactory();

        doGet(request,response);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


       EntityManager entityManager = MyEntityManager.getEntityManager();


        String query  = "SELECT v FROM Event v";

        List<Event> eventList;
        eventList = entityManager.createQuery(query)
                .getResultList();


        MyEntityManager.closeEntityManagerFactory();


        response.setContentType("application/json");
        response.setCharacterEncoding("utf-8");
        PrintWriter out = response.getWriter();



        Iterator<Event> eventIterator = eventList.iterator();
//
//
       JSONArray jsonArray = getEvents(eventIterator);


        out.print(jsonArray);
        out.flush();

    }


    private JSONArray getEvents(Iterator<Event> eventIterator){

        JSONArray jsonArray = new JSONArray();
        while (eventIterator.hasNext()){
            Event next_event = eventIterator.next();
            JSONObject jsonObject = new JSONObject();
            EntityManager entityManager = MyEntityManager.getEntityManager();

            entityManager.getTransaction().begin();

            String query  = "SELECT r FROM Path r where id = :id";

             Path p =  (Path)entityManager.createQuery(query)
                    .setParameter("id", next_event.getRouteId())
                    .getSingleResult();
            entityManager.getTransaction().commit();
            MyEntityManager.closeEntityManagerFactory();

            jsonObject.put("id", next_event.getId());
            jsonObject.put("description", next_event.getEventDescription());
            jsonObject.put("startingTime", next_event.getScheduledTime().toString());
            jsonObject.put("endTime", next_event.getExtepectedEndTime().toString());
            jsonObject.put("actualStartingTime", next_event.getScheduledTime().toString());
            jsonObject.put("actualEndTime", next_event.getExtepectedEndTime().toString());
            jsonObject.put("owner", next_event.getOwnerId());
            jsonObject.put("subscribers", EventData.getParticipants(next_event.getId()));
            jsonObject.put("eventStatus", next_event.getStatusId());
            jsonObject.put("eventPath", p.getDescription());
            jsonObject.put("ownerDetails", EventData.getOwner(next_event.getOwnerId()));

            jsonArray.add(jsonObject);

        }

        return jsonArray;
    }

}
