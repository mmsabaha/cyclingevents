package com.app;

import data.access.DummyDAO;
import models.User;
import models.UserEvent;
import org.json.simple.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.Date;

@WebServlet(name = "dummy")
public class DummyServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        UserEvent evt = new UserEvent();
        evt.setUserId(((User)request.getSession(false).getAttribute("user")).getId());
        evt.setEventId(1);
        evt.setCurrentLocation(request.getParameter("location"));
        evt.setCurrentStatus(Integer.parseInt(request.getParameter("status")));
        evt.setEventJoiningTime(new Timestamp(new Date().getTime()));
        evt.setEventSubscriptionTime(new Timestamp(new Date().getTime()));
        evt.setRemark(request.getParameter("message"));
        JSONObject obj = DummyDAO.updateStatus(evt);
        if ((boolean)obj.get("success")){
            out.print(obj);
        } else {
            out.print(obj);
        }
        out.flush();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
