package com.app;

import data.access.MyEntityManager;
import models.Path;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import javax.persistence.EntityManager;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

@WebServlet("/homePage")
public class HomePageServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        EntityManager entityManager = MyEntityManager.getEntityManager();

        entityManager.getTransaction().begin();

        String query  = "SELECT p FROM Path p";

        List<Path> pathList = entityManager.createQuery(query)
                .getResultList();


        entityManager.getTransaction().commit();
        MyEntityManager.closeEntityManagerFactory();

        response.setContentType("application/json");
        response.setCharacterEncoding("utf-8");
        PrintWriter out = response.getWriter();

        JSONArray jsonArray = new JSONArray();

        Iterator<Path> pathIterator = pathList.iterator();
//
//
        while (pathIterator.hasNext()){
            Path path = pathIterator.next();
            JSONObject jsonObject = new JSONObject();

            jsonObject.put("id", path.getId());
            jsonObject.put("description", path.getDescription());
            jsonObject.put("startingPoint", path.getStartingPoint());
            jsonObject.put("endPoint", path.getEndPoint());
            jsonObject.put("distanceInMiles", path.getDistanceInMiles());

            jsonArray.add(jsonObject);

        }


        out.print(jsonArray);
        out.flush();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
