package com.app;

import data.access.MyEntityManager;
import models.UserEvent;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;

@WebServlet("/joinEvent")
public class JoinEventServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        EntityManager entityManager = MyEntityManager.getEntityManager();

        entityManager.getTransaction().begin();

        String sql = "Select ue from UserEvent ue where eventId = :eventId and userId = :userId";


        UserEvent userEvent =  (UserEvent)entityManager.createQuery(sql)
                .setParameter("eventId", Integer.valueOf(request.getParameter("eventId")))
                .setParameter("userId",Integer.valueOf(request.getParameter("userId")))
                .getSingleResult();

        userEvent.setEventJoiningTime(new Timestamp(new java.util.Date().getTime()));

        entityManager.persist(userEvent);

        entityManager.getTransaction().commit();
        MyEntityManager.closeEntityManagerFactory();

        doGet(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
