package com.app;

import data.access.MyEntityManager;
import models.Event;

import javax.persistence.EntityManager;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/eventStatus")
public class EventStatusServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        updateEventStatus(Integer.valueOf(request.getParameter("eventId")), Integer.valueOf(request.getParameter("statusId")));

        doGet(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


    }

    private void updateEventStatus(int eventId, int statusId){

        EntityManager entityManager = MyEntityManager.getEntityManager();

        entityManager.getTransaction().begin();

        String query  = "SELECT event FROM Event event where event.id = :id";

        Event event = (Event) entityManager.createQuery(query).setParameter("id", eventId).getSingleResult();

        event.setStatusId(statusId);

        entityManager.persist(event);

        entityManager.getTransaction().commit();
        MyEntityManager.closeEntityManagerFactory();
    }

}
