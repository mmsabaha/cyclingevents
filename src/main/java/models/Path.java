package models;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Path {
    private int id;
    private String description;
    private String startingPoint;
    private String endPoint;
    private Integer distanceInMiles;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "description", nullable = true, length = 250)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "starting_point", nullable = true, length = 250)
    public String getStartingPoint() {
        return startingPoint;
    }

    public void setStartingPoint(String startingPoint) {
        this.startingPoint = startingPoint;
    }

    @Basic
    @Column(name = "end_point", nullable = true, length = 250)
    public String getEndPoint() {
        return endPoint;
    }

    public void setEndPoint(String endPoint) {
        this.endPoint = endPoint;
    }

    @Basic
    @Column(name = "distance_in_miles", nullable = true, precision = 0)
    public Integer getDistanceInMiles() {
        return distanceInMiles;
    }

    public void setDistanceInMiles(Integer distanceInMiles) {
        this.distanceInMiles = distanceInMiles;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Path path = (Path) o;

        if (id != path.id) return false;
        if (description != null ? !description.equals(path.description) : path.description != null) return false;
        if (startingPoint != null ? !startingPoint.equals(path.startingPoint) : path.startingPoint != null)
            return false;
        if (endPoint != null ? !endPoint.equals(path.endPoint) : path.endPoint != null) return false;
        if (distanceInMiles != null ? !distanceInMiles.equals(path.distanceInMiles) : path.distanceInMiles != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (startingPoint != null ? startingPoint.hashCode() : 0);
        result = 31 * result + (endPoint != null ? endPoint.hashCode() : 0);
        result = 31 * result + (distanceInMiles != null ? distanceInMiles.hashCode() : 0);
        return result;
    }
}
