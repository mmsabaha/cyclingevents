package models;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "user_event", schema = "cycling_events_db", catalog = "")
@IdClass(UserEventPK.class)
public class UserEvent {
    private int userId;
    private int eventId;
    private Timestamp eventSubscriptionTime;
    private Timestamp eventJoiningTime;
    private String currentLocation;
    private Integer currentStatus;
    private String remark;

    @Id
    @Column(name = "user_id", nullable = false)
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Id
    @Column(name = "event_id", nullable = false)
    public int getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    @Basic
    @Column(name = "event_subscription_time", nullable = true)
    public Timestamp getEventSubscriptionTime() {
        return eventSubscriptionTime;
    }

    public void setEventSubscriptionTime(Timestamp eventSubscriptionTime) {
        this.eventSubscriptionTime = eventSubscriptionTime;
    }

    @Basic
    @Column(name = "event_joining_time", nullable = true)
    public Timestamp getEventJoiningTime() {
        return eventJoiningTime;
    }

    public void setEventJoiningTime(Timestamp eventJoiningTime) {
        this.eventJoiningTime = eventJoiningTime;
    }

    @Basic
    @Column(name = "current_location", nullable = true, length = 100)
    public String getCurrentLocation() {
        return currentLocation;
    }

    public void setCurrentLocation(String currentLocation) {
        this.currentLocation = currentLocation;
    }

    @Basic
    @Column(name = "current_status", nullable = true)
    public Integer getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(Integer currentStatus) {
        this.currentStatus = currentStatus;
    }

    @Basic
    @Column(name = "remark", nullable = true, length = 100)
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserEvent userEvent = (UserEvent) o;

        if (userId != userEvent.userId) return false;
        if (eventId != userEvent.eventId) return false;
        if (eventSubscriptionTime != null ? !eventSubscriptionTime.equals(userEvent.eventSubscriptionTime) : userEvent.eventSubscriptionTime != null)
            return false;
        if (eventJoiningTime != null ? !eventJoiningTime.equals(userEvent.eventJoiningTime) : userEvent.eventJoiningTime != null)
            return false;
        if (currentLocation != null ? !currentLocation.equals(userEvent.currentLocation) : userEvent.currentLocation != null)
            return false;
        if (currentStatus != null ? !currentStatus.equals(userEvent.currentStatus) : userEvent.currentStatus != null)
            return false;
        if (remark != null ? !remark.equals(userEvent.remark) : userEvent.remark != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = userId;
        result = 31 * result + eventId;
        result = 31 * result + (eventSubscriptionTime != null ? eventSubscriptionTime.hashCode() : 0);
        result = 31 * result + (eventJoiningTime != null ? eventJoiningTime.hashCode() : 0);
        result = 31 * result + (currentLocation != null ? currentLocation.hashCode() : 0);
        result = 31 * result + (currentStatus != null ? currentStatus.hashCode() : 0);
        result = 31 * result + (remark != null ? remark.hashCode() : 0);
        return result;
    }
}
