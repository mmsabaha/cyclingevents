package data.access;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;


public class MyEntityManager {

    private static EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("cycling.club.wap.mum");

    public static void executeModel(Object model) {


        EntityManager entityManager = getEntityManager();

        entityManager.getTransaction().begin();
        entityManager.persist(model);


        entityManager.getTransaction().commit();
        entityManagerFactory.close();

    }

    public static EntityManager getEntityManager(){
        entityManagerFactory = Persistence.createEntityManagerFactory("cycling.club.wap.mum");
        return entityManagerFactory.createEntityManager();
    }

    public static void closeEntityManagerFactory(){

        entityManagerFactory.close();

    }

}
