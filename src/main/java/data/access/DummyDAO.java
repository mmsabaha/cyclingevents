package data.access;

import models.UserEvent;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.json.simple.JSONObject;
import util.HibernateUtil;

import java.util.HashMap;

public class DummyDAO {

    public static JSONObject updateStatus(UserEvent evt) {
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(evt);
            session.getTransaction().commit();
            HashMap<String, Object> map = new HashMap();
            map.put("success", true);
            map.put("message", "Registration successful.");
            return new JSONObject(map);
        } catch (HibernateException e) {
            System.out.println(e.getMessage());
            HashMap<String, Object> map = new HashMap();
            map.put("success", false);
            map.put("message", "Some error occurred.");
            return new JSONObject(map);
        }
    }

}
