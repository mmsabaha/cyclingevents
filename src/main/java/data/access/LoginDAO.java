package data.access;

import models.User;
import org.hibernate.Session;
import util.HibernateUtil;
import util.MD5;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

public class LoginDAO {

    public static User checkLogin(String username, String password){
        try {

            EntityManager entityManager = MyEntityManager.getEntityManager();
            entityManager.getTransaction().begin();
            String query  = "SELECT u FROM User u WHERE userName = :userName and password = :password";

            User user = (User) entityManager.createQuery(query)
                    .setParameter("userName", username)
                    .setParameter("password", MD5.toMD5(password))
                     .getSingleResult();
            System.out.println(user.getFirstName());
            return user;
        } catch (NoResultException e) {
            return null;
        }
    }
//    Session session = HibernateUtil.getSessionFactory().getCurrentSession();
//            session.beginTransaction();
//    User user = (User) session.createNativeQuery("SELECT * FROM user WHERE user_name = :username and password = :password")
//            .setParameter("username", username)
//            .setParameter("password", MD5.toMD5(password))
//            .addEntity(User.class)
//            .getSingleResult();
//            session.getTransaction().commit();
//            return user;
}
