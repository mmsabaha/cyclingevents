package data.access;

import models.User;
import org.hibernate.Session;
import org.json.simple.JSONObject;
import util.HibernateUtil;

import java.util.HashMap;
import java.util.List;

public class UserDAO {


    public static JSONObject getAllUsers() {
        JSONObject obj = null;
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            List<User> users = (List<User>) session.createNativeQuery("SELECT * FROM user")
                    .addEntity(User.class)
                    .getResultList();
            HashMap<String, Object> map = new HashMap<>();
            map.put("success", true);
            map.put("users", users);
            return new JSONObject(map);
        } catch (Exception e) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("success", false);
            map.put("users", null);
            return new JSONObject(map);
        }
    }

}
