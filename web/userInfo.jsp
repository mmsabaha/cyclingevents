<%--
  Created by IntelliJ IDEA.
  User: Behailu
  Date: 4/23/2018
  Time: 10:58 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Cycling Events</title>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.0/css/bulma.min.css">
    <link rel="stylesheet" href="css/home.css">
    <link rel="stylesheet" href="css/login.css">
    <script defer src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>
    <link href="images/bike.png" rel="shortcut icon"/>
    <script defer src="js/jquery.js"></script>
    <script defer src="js/login.js"></script>
    <%--<script src="homePage.js"></script>--%>
    <script src="js/userRegister.js"></script>
    <link rel="stylesheet" href="css/cyclist.css">
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
</head>
<body>

<section class="hero is-primary has-bg-img">
    <div class="navbar-end">
        <div class="navbar-item">
            <div class="field is-grouped">
                <p class="control" id="p-login">
                    <a class="button is-small is-info is-outlined" id="login-button">
                    <span class="icon">
                      <i class="fa fa-user"></i>
                    </span>
                        <span>Login</span>
                    </a>
                </p>
            </div>
        </div>
    </div>
    <div class="hero-body">
        <div class="container">
            <h1 class="title is-1">
                Cycling Events
            </h1>
            <h2 class="subtitle">
                Event Manager
            </h2>
            <div>Icons made by <a href="https://www.flaticon.com/authors/nikita-golubev" title="Bicycle">Bicycle</a>
                from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a> is licensed by <a
                        href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0"
                        target="_blank">CC 3.0 BY</a></div>
        </div>
    </div>
</section>

<section class="section">
    <div class="container">
        <div class="columns">

            <div class="column">
                <div class="card">
                    <form action="" method="post">
                    <div class="card-content">
                        <h2 class="title">Cyclist Registration Form</h2>
                        <%--<h3 class="subtitle">Please Fill the Form Below</h3>--%>

                        <div class="field">
                            <label class="label">First Name:</label>
                            <div class="control">
                                <input id="firstName" type="text" placeholder="First Name" class="input is-fullwidth" required/>
                            </div>
                        </div>

                        <div class="field">
                            <label class="label">Last Name:</label>
                            <div class="control">
                                <input id="lastName" type="text" placeholder="Last Name" class="input is-fullwidth" required/>
                            </div>
                        </div>

                        <div class="field">
                            <label class="label">Email:</label>
                            <div class="control">
                                <input id="email" type="text" placeholder="myname@example.com" pattern="[a-z0-9._+\-
]+@[a-z0-9.\-]+\.[a-z]{2,3}" class="input is-fullwidth" required/>
                            </div>
                        </div>

                        <div class="field">
                            <label class="label">Phone Number:</label>
                            <div class="control">
                                <input id="phone" type="text" placeholder="Phone Number" class="input is-fullwidth" pattern="\d{10}" required/>
                            </div>
                        </div>

                        <div class="field">
                            <label class="label">Address:</label>
                            <div class="control">
                                <input id="strAddress" type="text" placeholder="Street Address"
                                       class="input is-fullwidth" required/>
                                <input id="city" type="text" placeholder="City" class="input is-fullwidth" required/>
                                <input id="state" type="text" placeholder="State" class="input is-fullwidth" required/>
                                <input id="zipcode" type="text" placeholder="Zip Code" class="input is-fullwidth" required/>
                            </div>
                        </div>

                        <div class="field">
                            <label class="label">Club Name:</label>
                            <div class="control">
                                <div class="select is-fullwidth">
                                    <select id="event_routes">
                                        <option>[Select Club]</option>
                                    </select>
                                </div>
                            </div>
                        </div>


                        <div class="field">
                            <label class="label">Username:</label>
                            <div class="control">
                                <input id="username" type="text" placeholder="Username" class="input is-fullwidth" required/>
                            </div>

                        </div>

                        <div class="field">
                            <label class="label">Password:</label>
                            <div class="control">
                                <input id="password" type="password" placeholder="Password"
                                       pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
                                       title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters"
                                       class="input is-fullwidth" required/>
                            </div>

                        </div>
                        <div class="field">
                            <label class="label">Confirm Password:</label>
                            <div class="control">
                                <input id="confirmPassword" type="password" placeholder="Confirm Password"
                                       class="input is-fullwidth"/>
                            </div>

                        </div>

                        <div class="field">
                            <div class="control">
                                <label class="checkbox" id="termsContainer">
                                    <input type="checkbox" id="terms">
                                    I agree to the <a href="#">terms and conditions</a>
                                </label>
                            </div>
                        </div>


                        <footer class="card-footer">
                        <span class="card-footer-item">
                            <input type="submit" class="button is-primary" value="Submit"/>
                        </span>
                            <span class="card-footer-item">
                                <a href="" class="button is-danger">
                                <i class="fa fa-thumbs-o-down"></i>Reset
                            </a>
                            </span>

                        </footer>
                    </div>
                    </form>
                </div>
            </div>

            <div class="column">
                <div class="card">
                    <div class="card-content">
                        <h2 class="title">Available Events</h2>
                        <%--<h3 class="subtitle">Click here to view details</h3>--%>
                    </div>
                    <footer class="card-footer">
                        <%--<span class="card-footer-item">--%>
                            <%--<a href="#" class="button is-success">--%>
                                <%--<i class="fa fa-thumbs-o-up"></i>--%>
                            <%--</a>--%>
                        <%--</span>--%>
                        <%--<span class="card-footer-item">--%>
                            <%--<a href="#" class="button is-danger">--%>
                                <%--<i class="fa fa-thumbs-o-down"></i>--%>
                            <%--</a>--%>
                        <%--</span>--%>
                        <%--<span class="card-footer-item">--%>
                            <%--<a href="#" class="button is-info">--%>
                                <%--<i class="fa fa-retweet"></i>--%>
                            <%--</a>--%>
                        <%--</span>--%>
                    </footer>
                </div>

                <div class="image-loader">
                    <img src="http://mumstudents.org/cs472/2014-02/Lectures/ajax/loader.gif" alt="loader gif"/>
                </div>

            </div>
        </div>
    </div>
</section>

<div class="modal" id="modal-login">
    <div class="modal-background"></div>
    <div class="modal-card">
        <header class="modal-card-head">
            <p class="modal-card-title">Login</p>
            <button class="delete" aria-label="close" id="modal-close"></button>
        </header>
        <section class="modal-card-body">
            <!-- Content ... -->
            <div class="field">
                <div class="control">
                    <input id="username1" class="input is-primary" type="text" placeholder="Username">
                </div>
            </div>
            <div class="field">
                <div class="control">
                    <input id="password1" class="input is-primary" type="password" placeholder="Password">
                </div>
            </div>
            <div class="image-loader">
                <img src="http://mumstudents.org/cs472/2014-02/Lectures/ajax/loader.gif" alt="loader gif"/>
            </div>
        </section>
        <footer class="modal-card-foot pull-right">
            <button class="button is-success float-right" id="btn-login">Login</button>
            <p class="login-error" id="error-msg">Username/Password mistaken.</p>
        </footer>
    </div>
</div>


</div>
</div>

</body>
</html>

