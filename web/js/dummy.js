$(function () {
    $(".image-loader").hide();
    $(document).ajaxStart(function () {
        $(".image-loader").show();
    }).ajaxStop(function () {
        $(".image-loader").hide();
    });

    $('#error-msg').hide();

    $("#show-modal").on('click', function() {
        $('#modal-update-event').addClass("is-active");
    });

    $("#modal-close").click(function() {
        $('#modal-update-event').removeClass("is-active");
    });

    $('#btn-update').on('click', updateStatus);
});

function updateStatus() {
    const location = $('#location').val();
    const status = $('#status').val();
    const message = $('#message').val();
    $.ajax({
        "url": "dummy",
        "type": "POST",
        "data" : {
            "location" : location,
            "status" : status,
            "message" : message
        },
        "dataType": "json",
        "success": function (data) {
            // const error = $('#error-msg');
            if (data.success) {
                alert("Your status has been posted");
            } else {
                alert("Could not post your status");
            }
        },
        "error": function (a, b, c) {

        }

    });
}