"use strict"
//Author Mrisho
$(function () {

    const loader = $("#loader_img");
    const left = $(document).width()/2;
    const top = $(document).height()/2;



    loader.hide();
    $(document).ajaxStart(function () {

        loader.css({
            "position": "fixed",
            "left" : 0 + "px",
            "top" : 0 + "px",
            "display": "inline-block",
            "width" : "30px",
            "height" : "30px",
              "margin" : "auto"

        });
    }).ajaxStop(function () {
       loader.hide();
    });

    $.ajax({
        "url": "homePage",
        "type": "GET",
        "data" : {

        },
        "dataType": "json",
        "success": function (data) {

            $.each(data, function (i,d) {

                $("<option>").val(d.id).text(d.description).appendTo("#event_routes");
            })

        },
        "error": function (a, b, c) {
            console.log("error");
            console.log(a, b, c);
        }

    });
});