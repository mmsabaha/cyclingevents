<%--
  Created by IntelliJ IDEA.
  User: Behailu
  Date: 4/23/2018
  Time: 11:06 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Cycling Events</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.0/css/bulma.min.css">
    <script defer src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>
    <link href="images/bike.png" rel="shortcut icon"/>
    <link rel="stylesheet" href="css/events.css">
</head>
<body>
<section class="hero is-info is-large">
    <div class="hero-head">
        <nav class="navbar">
            <div class="container">
                <div class="navbar-brand">
                    <a class="navbar-item">
                        <h1 class="title is-1">Cycling Event Registration Form</h1>
                    </a>
                    <span class="navbar-burger burger" data-target="navbarMenuHeroB">
            <span></span>
            <span></span>
            <span></span>
          </span>
                </div>
                <div id="navbarMenuHeroB" class="navbar-menu">
                    <div class="navbar-end">
                        <a class="navbar-item">
                            Home
                        </a>
                        <a class="navbar-item">
                            Live Cycling Rides
                        </a>
                        <a class="navbar-item" href="index.jsp">
                            Logout
                        </a>

                    </div>
                </div>
            </div>
        </nav>
    </div>
</section>
<div id="container">
    <br>
    <div class="control">
        <label class="label">Event Description:</label>
        <textarea id="eventDescription"  class="textarea" placeholder="e.g. Write Event Description here"></textarea>
    </div>
    <br>
    <div class="control">
        <label class="label">Select Route:</label>
        <select name="route" id="route">
            <option value="Club">Select Route</option>
        </select>
    </div>
    <br>
    <div class="control">
        <label class="label">Event Scheduled Date and Time :</label>
        <input id="scheduledDate"  type="date"></input>  &nbsp; <input id="scheduledTime"  type="time"></input>
    </div>
    <br>
    <div class="control">
        <label class="label">Event Expected End Date and Time :</label>
        <input id="expectedEndDate"  type="date"></input>  &nbsp; <input id="expectedEndTime"  type="time"></input>
    </div>
    <br>


        <div class="field is-grouped">
            <div class="control">
                <button id="submit" class="button is-link">Register</button>
            </div>
            <a class="button" href="index.jsp">
                <div id="cancel" class="control">
                    <button class="button is-text" >Cancel</button>
                </div>
            </a>
        </div>

    </div>

</div>
</body>
</html>
