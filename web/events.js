"use strict"
///Author: Mrisho
$(function () {
//Temporarily
    let currentUserId = 1;

    const registerForm = $("#registerForm");
    const seeAvEvents = $("#contentEvents");
    const allUsers = $("#systemUsers");
    const btnAvailable = $("#available");
    const btnRegister = $("#register");
    const btnUsers = $("#seeUsers");
    const eventFormFooter =  $(".register-event-footer");
    const emergencyBar = $("#emergencyBar");
    emergencyBar.hide();

    let eventObjects;

    eventFormFooter.hide();

    //format date
    function formatDate(date) {
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var ampm = hours >= 12 ? 'pm' : 'am';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0'+minutes : minutes;
        var strTime = hours + ':' + minutes + ' ' + ampm;
        return date.getMonth()+1 + "/" + date.getDate() + "/" + date.getFullYear() + "  " + strTime;
    }


    registerForm.hide();
    allUsers.hide();

    //load logged in user, get id
    $.ajax({
        "url": "login",
        "type": "GET",
        "data" : {

        },
        "dataType": "json",
        "success": function (data) {

          currentUserId = data.id;
        },
        "error": function (a, b, c) {
            console.log("error");
            console.log(a, b, c);
        }

    });

    //menu buttons click events

    $("#available").on("click",function () {
        registerForm.hide();
        seeAvEvents.show();
        allUsers.hide();
        eventFormFooter.hide();
        btnAvailable.addClass("is-success").removeClass("is-info");
        btnRegister.addClass("is-info").removeClass("is-success");
        btnUsers.addClass("is-info").removeClass("is-success");
    });

    $("#register").on("click", function () {
        registerForm.show();
        seeAvEvents.hide();
        allUsers.hide();
        eventFormFooter.show();
        btnRegister.addClass("is-success").removeClass("is-info");
        btnAvailable.addClass(" is-info").removeClass("is-success");
        btnUsers.addClass(" is-info").removeClass("is-success");
    });

    $("#seeUsers").on("click", function () {
        registerForm.hide();
        seeAvEvents.hide();
        allUsers.show();
        eventFormFooter.hide();
        btnUsers.addClass("is-success").removeClass("is-info");
        btnRegister.addClass("is-info").removeClass("is-success");
        btnAvailable.addClass("is-info").removeClass("is-success");
    });

    function formatEventDetail(eventData){

        let td = $("<td>");

        td.html("<em><strong>Owner:</strong>"  + eventData.ownerDetails.firstName + " " + eventData.ownerDetails.lastName + "</em><br>"  +
            "<strong>Description:</strong>" +  eventData.description + "<br><strong> Path :</strong>" + eventData.eventPath +
            "<br><strong>Starting Time : </strong>" + formatDate(new Date(eventData.startingTime)) + "<br><strong> Ending Time: </strong>" + formatDate(new Date(eventData.endTime)));
         return td;
    }
    //load all events function, to be called when want to refresh data
    function loadEvents( data) {

        let emergency = false;

        $.each(data, function (i, d) {
             console.log(d.owner);
             console.log(currentUserId);
            if (d.owner === currentUserId && d.eventStatus !==4) {
                let row = $("<tr>");
                let button = $("<button>");
                let viewButton = $("<button>");
                button.attr("id", d.id);
                viewButton.addClass("button").addClass("is-info").addClass("view").text("View");
                viewButton.attr("id","view_" + d.id);
                let lastDt = $("<td>");
                let lastDtView = $("<td>");

                if(d.eventStatus === 1){

                    button.text("Start").addClass("is-fullwidth");
                    button.addClass("button").addClass("is-danger").addClass("startEvent");
                    viewButton.appendTo(lastDtView);
                    button.appendTo(lastDt);
                    formatEventDetail(d).appendTo(row);
                    lastDt.appendTo(row);
                    lastDtView.appendTo(row);

                    row.appendTo($("#tableAvailable"));

                }
                else if(d.eventStatus === 2 || d.eventStatus === 3  ) {

                    if( d.eventStatus === 3 ){
                        row.css("color", "red");
                        button.text("Proceed").addClass("is-fullwidth");
                        button.addClass("button").addClass("is-success").addClass("proceed").removeClass("reportEmergency");
                        emergency = true;
                    } else {

                        row.css("color", "black");
                        button.text("Emergency").addClass("is-fullwidth");
                        button.addClass("button").addClass("is-danger").addClass("reportEmergency").removeClass("proceed");
                    }


                    button.appendTo(lastDt);
                    viewButton.appendTo(lastDtView);

                    formatEventDetail(d).appendTo(row);

                    lastDt.appendTo(row);
                    lastDtView.appendTo(row);
                    row.appendTo($("#liveEvents"));
                }


            }

        });

        $.each(data, function (i, d) {
            if (d.owner !== currentUserId && d.eventStatus !== 4) {
                let row = $("<tr>");
                let button = $("<button>");
                button.attr("id", d.id);
                let lastDt = $("<td>");
                let viewButton = $("<button>");
                viewButton.addClass("button").addClass("is-info").addClass("view").text("View");
                viewButton.attr("id","view_" + d.id);

                let lastDtView = $("<td>");
                if(d.eventStatus === 1) {

                    button.text("Subscribe").addClass("is-fullwidth");
                    button.addClass("button").addClass("is-success").addClass("subscribe");

                    $.each(d.subscribers, function (j,k) {

                        if(k.id === currentUserId){

                            button.removeClass("is-success").addClass("is-primary")

                                .addClass("unsubscribe").text("un subscribe")
                                .removeClass("subscribe");
                        }
                    });



                    viewButton.appendTo(lastDtView);
                    button.appendTo(lastDt);
                    formatEventDetail(d).appendTo(row);
                    lastDt.appendTo(row);
                    lastDtView.appendTo(row);
                    row.appendTo($("#tableAvailable"));

                } else   if(d.eventStatus === 2 || d.eventStatus === 3 ){

                    if( d.eventStatus === 3 ){
                        row.css("color", "red");
                        emergency = true;
                    } else {
                        row.css("color", "black");
                    }

                    button.text("Join").addClass("is-fullwidth");
                    button.addClass("button").addClass("is-success").addClass("join");
                    button.hide();
                    $.each(d.subscribers, function (j,k) {

                        if(k.id === currentUserId){
                            button.show();

                            if(k.subscription.joiningTime.length > 0){

                                button.removeClass("is-success").addClass("is-info")

                                    .addClass("updateLocation").text("update location")
                                    .removeClass("join");
                            }

                        }
                    });

                    viewButton.appendTo(lastDtView);
                    button.appendTo(lastDt);
                    formatEventDetail(d).appendTo(row);
                    lastDt.appendTo(row);
                    lastDtView.appendTo(row);
                    row.appendTo($("#liveEvents"));
                }


            }
        });

        if(emergency)
            emergencyBar.show();
        else
            emergencyBar.hide();
    }



    $("#submitEvent").on("click", function (evt) {

    $.ajax({
        "url": "event",
        "type": "POST",
        "data" : {

            "description" : $("#description").val(),
            "route" : $("#event_routes").val(),
            "startTime" : $("#expectedStartTime").val(),
            "endTime" : $("#expectedEnd").val()

        },
        "dataType": "json",
        "success": function (data) {

            console.log(data);


            registerForm.hide();
            seeAvEvents.show();
            allUsers.hide();
            eventFormFooter.hide();
            btnAvailable.addClass("is-success").removeClass("is-info");
            btnRegister.addClass("is-info").removeClass("is-success");
            btnUsers.addClass("is-info").removeClass("is-success");
            $("#liveEvents").empty();
            $("#tableAvailable").empty();
            eventObjects = data;
            loadEvents(data);
        },
        "error": function (a, b, c) {
            console.log("error");
            console.log(a, b, c);
        }

    });
evt.preventDefault();
});

//getting event data and call load event function(data)
function refreshEvents() {
    $.ajax({
        "url": "event",
        "type": "GET",
        "data": {},
        "dataType": "json",
        "success": function (data) {
         //   console.log(data);

            $("#liveEvents").empty();
            $("#tableAvailable").empty();
          // eventObjects = data;
            loadEvents(data);

        },
        "error": function (a, b, c) {
            console.log("error");
            console.log(a, b, c);
        }

    });
}
    refreshEvents();

    function changeEventStatus(eventId, statusId){

    $.ajax({
        "url": "eventStatus",
        "type": "POST",
        "data": {
            "eventId" : eventId,
            "statusId" : statusId
        },
        "dataType": "json",
        "success": function (data) {

            refreshEvents();

        },
        "error": function (a, b, c) {
            console.log("error");
            console.log(a, b, c);
        }

    });
}



    ///////////////Event Status Change
    $(document).on("click", ".subscribe", function(evt){

        let button = $(evt.target);
        let eventId = button.attr("id");
        button.removeClass("is-success").addClass("is-primary")

            .addClass("unsubscribe").text("un subscribe")
            .removeClass("subscribe");

        $.ajax({
            "url": "eventSubscription",
            "type": "POST",
            "data": {
                "eventId" : eventId,
                "userId" : currentUserId
            },
            "dataType": "json",
            "success": function (data) {

                refreshEvents();

            },
            "error": function (a, b, c) {
                console.log("error");
                console.log(a, b, c);
            }

        });
        evt.stopPropagation();

        console.log(eventId);
        // refreshEvents();
    });

    $(document).on("click", ".startEvent", function(evt){

        let button = $(evt.target);
        let eventId = button.attr("id");


        evt.stopPropagation();

        changeEventStatus(eventId, 2);
      //  console.log(eventId);
        // refreshEvents();
    });

    $(document).on("click", ".reportEmergency", function(evt){

        let button = $(evt.target);
        let eventId = button.attr("id");
        button.removeClass("is-success").addClass("is-primary")

            .addClass("proceed").text("Proceed")
            .removeClass("reportEmergency");

        evt.stopPropagation();
        changeEventStatus(eventId, 3);
      //  console.log(eventId);
        // refreshEvents();
        emergencyBar.show();
    });

    $(document).on("click", ".proceed", function(evt){

        let button = $(evt.target);
        let eventId = button.attr("id");


        evt.stopPropagation();
        changeEventStatus(eventId, 2);
      //  console.log(eventId);
        // refreshEvents();
        emergencyBar.show();
    });

    $(document).on("click", ".join", function(evt){

        let button = $(evt.target);
        let eventId = button.attr("id");
        $.ajax({
            "url": "joinEvent",
            "type": "POST",
            "data": {
                "eventId" : eventId,
                "userId" : currentUserId
            },
            "dataType": "json",
            "success": function (data) {

                refreshEvents();

            },
            "error": function (a, b, c) {
                console.log("error");
                console.log(a, b, c);
            }

        });

        evt.stopPropagation();
        console.log(eventId);
        // refreshEvents();
    });

    $(document).on("click", ".view", function(evt){
        registerForm.hide();
        seeAvEvents.hide();
        allUsers.show();
        eventFormFooter.hide();
        btnUsers.addClass("is-success").removeClass("is-info");
        btnRegister.addClass("is-info").removeClass("is-success");
        btnAvailable.addClass("is-info").removeClass("is-success");

        let button = $(evt.target);
        let eventId = button.attr("id");
        eventId = eventId.split("_")[1];

        $.ajax({
            "url": "subscribers",
            "type": "GET",
            "data": {
                "eventId": eventId
            },
            "dataType": "json",
            "success": function (data) {
                console.log(data);
                $("#listOfUsers").empty();
                 $.each(data, function(a,k){


                     let row = $("<tr>");
                     $("<td>").text(k.firstName).appendTo(row);
                     $("<td>").text(k.lastName).appendTo(row);
                     $("<td>").text(k.currentLocation).appendTo(row);

                     row.appendTo($("#listOfUsers"));
                 });


            },
            "error": function (a, b, c) {
                console.log("error");
                console.log(a, b, c);
            }

        });


        evt.stopPropagation();
        console.log(eventId);
        // refreshEvents();
    });


});