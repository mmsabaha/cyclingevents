<%--
  Created by IntelliJ IDEA.
  User: Behailu
  Date: 4/22/2018
  Time: 7:55 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Cycling Events</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.0/css/bulma.min.css">
    <script defer src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>
    <link href="images/bike.png" rel="shortcut icon"/>
    <link rel="stylesheet" href="css/cyclist.css">
</head>
<body>
<section class="hero is-primary">
    <div class="hero-body">
        <div class="container">
            <h1 class="title is-1">Cyclist Registration Form</h1>
        </div>

    </div>
</section>
<br>
<div id="container">
    <div class="control">
        <label class="label">Name:</label>
        <input id="firstName"  type="text" placeholder="First Name"> &nbsp; <input id="lastName" type="text" placeholder="Last Name">
    </div>
    <br>
    <div class="control">
        <label class="label">Email:</label>
        <input id="email" type="text" placeholder="myname@example.com" pattern="[a-z0-9._+\-
]+@[a-z0-9.\-]+\.[a-z]{2,3}">
    </div>
    <br>
    <div class="control">
        <label class="label">Phone Number:</label>
        <input id="phoneNumber" type="text" placeholder="Area Code"> &nbsp; <input  type="text" placeholder="Phone Number">
    </div>
    <br>
    <div class="control">
        <label class="label">Address:</label>
        <input id="strAddr" type="text" placeholder="Street Address"> &nbsp; <input id="city"  type="text" placeholder="City">
        <br><br><input id="state" type="text" placeholder="State"> &nbsp; <input id="zipcode"  type="text" placeholder="Zip Code">
    </div>
    <br>
    <div class="control">
        <label class="label">Club Name:</label>
        <select name="club" id="club">
            <option value="Club">Select Club</option>
        </select>
    </div>
    <br>
    <div class="control">
        <label class="label">Username:</label>
        <input id="username" type="text" placeholder="Username">
    </div>
    <br>
    <div class="control">
        <label class="label">Password:</label>
        <input  id="password" type="password" placeholder="Password"  pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters">
        &nbsp; <input id="confirmPassword" type="password" placeholder="Confirm Password">
    </div>
    <br>
    <div>
        <div class="field">
            <div class="control">
                <label class="checkbox">
                    <input id="agree" type="checkbox">
                    I agree to the <a href="#">terms and conditions</a>
                </label>
            </div>
        </div>


        <div class="field is-grouped">
            <div class="control">
                <button id="submit" class="button is-link">Submit</button>
            </div>
            <a class="button" href="index.jsp">
            <div id="cancel" class="control">
                <button class="button is-text" >Cancel</button>
            </div>
            </a>
        </div>

    </div>

</div>



</body>
</html>
